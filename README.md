# Time Logger

This is random python script designed with precision to
help with creation of world peace by providing eazy
and comfortable way of logging work time to some
private website.

To log time use:
```bash
$ ./log_time.py
```

By default script logs 8 hours and writes general message that should be better rewritten.

To override default settings, script provdes following args:
```bash
usage: log_time.py [-h] [-t TIME] [-m MSG] [-d DATE]

optional arguments:
  -h, --help                show this help message and exit
  -t TIME, --log-time TIME  time to be logged (default: 8)
  -m MSG, --message MSG     descriptive message for logged time
                            (default: I finished the stuff from yesterday.)
  -d DATE, --date DATE      date to be used (default: $(date +"%Y-%M-%d"))
```

Script provides some configuration in `config.py` module.
