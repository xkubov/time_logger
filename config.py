# Relevant URLs
BASE_URL = "http://retdecdb.cz.avg.com/redmine"
LOGIN_URL = BASE_URL+"/login"
LOGTIME_URL = BASE_URL+"/projects/decompiler/time_entries/new"

# Timeot for page load
TIMEOUT = 5

# Format of date used
CORE_DATE_FORMAT = "%Y-%m-%d"
DATE_FORMAT = "%Y-%m-%d"

# Site specifics
MAX_HOURS = 16
MAX_MESSAGE_LEN = 256
