import sys


class ProgressLogger:
    def __init__(self, job_name="job", job_repeat=1, verbose=True):
        self._total = job_repeat
        self._progress = 0
        self._job = job_name
        self._verbose = verbose

    def __enter__(self):
        self._print_progress()
        return self

    def __exit__(self, type, value, traceback):
        self.complete()

    def _print_out(self, msg):
        if self._verbose:
            sys.stdout.write(msg)
            sys.stdout.flush()

    def _print_progress(self):
        length = 20  # modify this to change the length
        block = int(round(length*self._progress))
        msg = "\r{0}: [{1}] {2}%".format(
                self._job,
                "#"*block+"-"*(length-block),
                round(self._progress*100, 2)
        )
        self._print_out(msg)

    def track_job(self, job_name, t):
        if t < 0:
            self._total = 0
        else:
            self._total = t

        self._progress = 0
        self._job = job_name
        self._print_progress()

    def progress(self):
        if self._total:
            self._progress += 1.0/self._total

        if self._progress > 1.0:
            self._progress = 1.0

        self._print_progress()

    def complete(self):
        self._progress = 1.0
        self._print_progress()
        self._print_out(" DONE\r\n")
        self._total = 0
        self._progress = 0
