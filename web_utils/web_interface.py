import sys

import mechanicalsoup


class WebWrapper():
    def __init__(self):
        self._browser = mechanicalsoup.StatefulBrowser()

    def _check_present(self, element='div', **specs):
        page = self._browser.get_current_page()
        return True if page.find(element, **specs) else False

    def fill_form(self, url, form_id, submit_id, **kwargs):
        self._browser.open(url)
        self._browser.select_form(form_id)
        for id, value in kwargs.items():
            self._browser[id] = value

        self._browser.submit_selected(btnName=submit_id)
