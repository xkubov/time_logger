#!/usr/bin/env python3

import config

import argparse
import datetime
import getpass
import sys

from requests.exceptions import ConnectionError

from web_utils.web_interface import WebWrapper


class TimeLogger(WebWrapper):
    def _check_logged_successful(self):
        if self._check_present(class_="flash error"):
            sys.stderr.write('Permission denied.')
            sys.exit(1)

    def logTime(self, time, message, date):
        self.fill_form(
            url=config.LOGTIME_URL,
            form_id='#new_time_entry',
            submit_id='commit',
            **{
                "time_entry[spent_on]": date.strftime(config.CORE_DATE_FORMAT),
                "time_entry[hours]": str(time),
                "time_entry[comments]": message
            }
        )

    def login(self, username, password):
        self.fill_form(
            url=config.LOGIN_URL,
            form_id='form[action="/redmine/login"]',
            submit_id='login',
            username=username,
            password=password,
        )
        self._check_logged_successful()


def check_args(args):
    if args.date:
        try:
            args.date = datetime.datetime.strptime(
                args.date,
                config.DATE_FORMAT
            ).date()

        except ValueError:
            print("Error: ", args.date+': not valid date. Use',
                  config.DATE_FORMAT + ' format.')

            sys.exit(1)

    if args.time > config.MAX_HOURS:
        print("Error: You cannot log", args.time, "hours.",
              "(Maximum is", config.MAX_HOURS, "hours.)")

        sys.exit(1)

    if len(args.message) > config.MAX_MESSAGE_LEN:
        print("Error: Message may not be larger than",
              config.MAX_MESSAGE_LEN,
              "characters. (actual: {})".format(len(args.message)))

        sys.exit(1)


def parse_args(args):
    parser = argparse.ArgumentParser(
                description=__doc__,
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-t', '--log-time',
                        type=float,
                        dest='time', default=8,
                        metavar='TIME',
                        help='time to be logged')

    parser.add_argument('-m', '--message',
                        dest='message',
                        type=str,
                        metavar='MSG',
                        help='descriptive message for logged time',
                        default='I finished the stuff from yesterday.')

    parser.add_argument('-d', '--date',
                        dest='date',
                        type=str,
                        metavar='DATE',
                        help='date to be used',
                        default=datetime.date.today().strftime(
                            config.DATE_FORMAT
                        ))

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    check_args(args)

    user = getpass.getuser()
    password = getpass.getpass()

    logger = TimeLogger()

    try:
        logger.login(user, password)
        logger.logTime(args.time, args.message, args.date)

    except ConnectionError:
        sys.stderr.write('Connection error: internal network?')
        sys.exit(1)

    except Exception as e:
        sys.stderr.write("\nError: "+str(e))
        sys.exit(1)
